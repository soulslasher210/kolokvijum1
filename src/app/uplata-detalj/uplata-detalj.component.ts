import { Racun } from './../racun';
import { ServisService } from './../servis.service';
import { Uplata } from './../uplata';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-uplata-detalj',
  templateUrl: './uplata-detalj.component.html',
  styleUrls: ['./uplata-detalj.component.css']
})
export class UplataDetaljComponent implements OnInit {

  item: Uplata = {} as Uplata;
  items2:Racun[] = []
  izmeniForm;

  constructor(
    private service: ServisService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.dobaviSve();
  }

  dobaviSve() {
    this.service
      .detaljUplata(this.route.snapshot.params['id'])
      .subscribe((data) => {
        this.item = data;

        this.izmeniForm = this.formBuilder.group({
          id: this.item.id,
          uplatilac: this.item.uplatilac,
          primalac: this.item.primalac,
          iznos:this.item.iznos
        } as Uplata);

        this.service.dobaviRacun().subscribe(data=>this.items2=data);

        
      });
  }

  izmeni(item) {
    item.uplatilac = parseInt(item.uplatilac);
    item.primalac = parseInt(item.primalac);
    this.service.izmeniUplatu(item).subscribe((data) => (this.item = data));
  }

}
