import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BankaComponent } from './banka/banka.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { BankaDetaljComponent } from './banka-detalj/banka-detalj.component';
import { UplataComponent } from './uplata/uplata.component';
import { UplataDetaljComponent } from './uplata-detalj/uplata-detalj.component';
import { RacunComponent } from './racun/racun.component';
import { RacunDetaljComponent } from './racun-detalj/racun-detalj.component';
import { BrzoPlacanjeComponent } from './brzo-placanje/brzo-placanje.component';


@NgModule({
  declarations: [
    AppComponent,
    BankaComponent,
    BankaDetaljComponent,
    UplataComponent,
    UplataDetaljComponent,
    RacunComponent,
    RacunDetaljComponent,
    BrzoPlacanjeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
