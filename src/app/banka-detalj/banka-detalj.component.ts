import { Banka } from './../banka';
import { ServisService } from './../servis.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-banka-detalj',
  templateUrl: './banka-detalj.component.html',
  styleUrls: ['./banka-detalj.component.css']
})
export class BankaDetaljComponent implements OnInit {
  item: Banka = {} as Banka;
  izmeniForm;

  constructor(
    private service: ServisService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.dobaviSve();
  }

  dobaviSve() {
    this.service
      .detaljBanka(this.route.snapshot.params['id'])
      .subscribe((data) => {
        this.item = data;

        this.izmeniForm = this.formBuilder.group({
          id: this.item.id,
          poslovnoIme: this.item.poslovnoIme,
          pib: this.item.pib,
        } as Banka);

        
      });
  }

  izmeni(item) {
    this.service.izmeniBanku(item).subscribe((data) => (this.item = data));
  }
}
