import { Banka } from './banka';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Uplata } from './uplata';
import { Racun } from './racun';

@Injectable({
  providedIn: 'root'
})
export class ServisService {

  adresa:string = "http://localhost:3000/"; 

  constructor(private http:HttpClient) { }

  dobaviBanku():Observable<Banka[]>{
    return this.http.get<Banka[]>(this.adresa+"banka");
  }
  obrisiBanku(id):Observable<Banka>{
    return this.http.delete<Banka>(this.adresa+"banka/"+id);
  }
  dodajBanku(item):Observable<Banka>{
    return this.http.post<Banka>(this.adresa + "banka", item);
  }
  detaljBanka(id):Observable<Banka>{
    return this.http.get<Banka>(this.adresa+"banka/"+id);
  }
  izmeniBanku(item):Observable<Banka>{
    return this.http.put<Banka>(this.adresa+"banka/"+item.id,item);
  }

  dobaviUplatu():Observable<Uplata[]>{
    return this.http.get<Uplata[]>(this.adresa+"uplata");
  }
  obrisiUplatu(id):Observable<Uplata>{
    return this.http.delete<Uplata>(this.adresa+"uplata/"+id);
  }
  dodajUplatu(item):Observable<Banka>{
    return this.http.post<Banka>(this.adresa + "uplata", item);
  }
  detaljUplata(id):Observable<Uplata>{
    return this.http.get<Uplata>(this.adresa+"uplata/"+id);
  }
  izmeniUplatu(item):Observable<Uplata>{
    return this.http.put<Uplata>(this.adresa+"uplata/"+item.id,item);
  }



  dobaviRacun():Observable<Racun[]>{
    return this.http.get<Racun[]>(this.adresa+"racun");
  }
  obrisiRacun(id):Observable<Racun>{
    return this.http.delete<Racun>(this.adresa+"racun/"+id);
  }
  dodajRacun(item):Observable<Racun>{
    return this.http.post<Racun>(this.adresa + "racun", item);
  }
  detaljRacun(id):Observable<Racun>{
    return this.http.get<Racun>(this.adresa+"racun/"+id);
  }
  izmeniRacun(item):Observable<Racun>{
    return this.http.put<Racun>(this.adresa+"racun/"+item.id,item);
  }

}
