import { Racun } from './../racun';
import { ServisService } from './../servis.service';
import { Uplata } from './../uplata';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-uplata',
  templateUrl: './uplata.component.html',
  styleUrls: ['./uplata.component.css']
})
export class UplataComponent implements OnInit {
  

  items:Uplata[]=[];
  items2:Racun[]=[];
  dodajForm;

  constructor(
    private servis:ServisService,
    private router:Router,
    private formBuilder:FormBuilder
  ) {
    this.dodajForm = this.formBuilder.group({
      id: 0,
      uplatilac: 0,
      primalac: 0,
      iznos:0
    }as Uplata);
   }

  ngOnInit(): void {
    this.dobaviSve();
  }

  
  dobaviSve(){
    this.servis.dobaviUplatu().subscribe(data=>this.items=data);
    this.servis.dobaviRacun().subscribe(data=>this.items2=data);
  }
  obrisi(id){
    this.servis.obrisiUplatu(id).subscribe(data=>this.dobaviSve());
  }
  dodaj(item){
    item.uplatilac = parseInt(item.uplatilac);
    item.primalac = parseInt (item.primalac);
    this.servis.dodajUplatu(item).subscribe(data=>this.dobaviSve());
  }
  detalj(id){
    this.router.navigate(["uplata/uplataDetalj",{id:id}]);
  }

}
