import { RacunDetaljComponent } from './racun-detalj/racun-detalj.component';
import { RacunComponent } from './racun/racun.component';
import { UplataDetaljComponent } from './uplata-detalj/uplata-detalj.component';
import { UplataComponent } from './uplata/uplata.component';
import { BankaDetaljComponent } from './banka-detalj/banka-detalj.component';
import { BankaComponent } from './banka/banka.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {path:"banka",component:BankaComponent},
  {path:"banka/bankaDetalj",component:BankaDetaljComponent},
  {path:"uplata",component:UplataComponent},
  {path:"uplata/uplataDetalj",component:UplataDetaljComponent},
  {path:"racun",component:RacunComponent},
  {path:"racun/racunDetalj",component:RacunDetaljComponent},
  


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
