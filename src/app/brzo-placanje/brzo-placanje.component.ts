import { ServisService } from './../servis.service';
import { Racun } from './../racun';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-brzo-placanje',
  templateUrl: './brzo-placanje.component.html',
  styleUrls: ['./brzo-placanje.component.css']
})
export class BrzoPlacanjeComponent implements OnInit {
  item:Racun = {} as Racun;
  izmeniForm;
  uplata:number;
  ukupno:number;
  constructor(
    private service: ServisService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.dobaviSve(2);
  }

  dobaviSve(brojRacuna) {
    this.service
      .detaljRacun(brojRacuna)
      .subscribe((data) => {
        this.item = data;

        this.izmeniForm = this.formBuilder.group({
          id: this.item.id,
          bankaId: this.item.bankaId,
          brojRacuna: this.item.brojRacuna,
          stanje:this.item.stanje
        } as Racun);

       

        
      });
  }

  izmeni(item) {
    this.service.izmeniRacun(item).subscribe((data) => (this.item = data));
  }
  onKey(event:any){
    this.uplata += this.ukupno;
  }



}
