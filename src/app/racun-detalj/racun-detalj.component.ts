import { Banka } from './../banka';
import { ServisService } from './../servis.service';
import { Component, OnInit } from '@angular/core';
import { Racun } from '../racun';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-racun-detalj',
  templateUrl: './racun-detalj.component.html',
  styleUrls: ['./racun-detalj.component.css']
})
export class RacunDetaljComponent implements OnInit {
  item: Racun = {} as Racun;
  items2:Banka[] = []
  izmeniForm;

  constructor(
    private service: ServisService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.dobaviSve();
  }

  dobaviSve() {
    this.service
      .detaljRacun(this.route.snapshot.params['id'])
      .subscribe((data) => {
        this.item = data;

        this.izmeniForm = this.formBuilder.group({
          id: this.item.id,
          bankaId: this.item.bankaId,
          brojRacuna: this.item.brojRacuna,
          stanje:this.item.stanje
        } as Racun);

        this.service.dobaviBanku().subscribe(data=>this.items2=data);

        
      });
  }

  izmeni(item) {
    item.bankaId = parseInt(item.bankaId);
    this.service.izmeniRacun(item).subscribe((data) => (this.item = data));
  }
}
