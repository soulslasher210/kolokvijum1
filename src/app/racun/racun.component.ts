import { Banka } from './../banka';
import { ServisService } from './../servis.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Racun } from '../racun';


@Component({
  selector: 'app-racun',
  templateUrl: './racun.component.html',
  styleUrls: ['./racun.component.css']
})
export class RacunComponent implements OnInit {
  items:Racun[]=[];
  items2:Banka[]=[];
  dodajForm;

  constructor(
    private servis:ServisService,
    private router:Router,
    private formBuilder:FormBuilder
  ) {
    this.dodajForm = this.formBuilder.group({
      id: 0,
      bankaId: 0,
      brojRacuna: "",
      stanje:0
    }as Racun);
   }

  ngOnInit(): void {
    this.dobaviSve();
  }

  
  dobaviSve(){
    this.servis.dobaviRacun().subscribe(data=>this.items=data);
    this.servis.dobaviBanku().subscribe(data=>this.items2=data);
  }
  obrisi(id){
    this.servis.obrisiRacun(id).subscribe(data=>this.dobaviSve());
  }
  dodaj(item){
    item.bankaId = parseInt(item.bankaId);
    this.servis.dodajRacun(item).subscribe(data=>this.dobaviSve());
  }
  detalj(id){
    this.router.navigate(["racun/racunDetalj",{id:id}]);
  }

}
