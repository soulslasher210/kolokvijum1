import { ServisService } from './../servis.service';
import { Banka } from './../banka';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-banka',
  templateUrl: './banka.component.html',
  styleUrls: ['./banka.component.css']
})
export class BankaComponent implements OnInit {
  items:Banka[]=[];
  dodajForm;
  constructor(
    private servis:ServisService,
    private router:Router,
    private formBuilder:FormBuilder

  ) {

    this.dodajForm = this.formBuilder.group({
      id: 0,
      poslovnoIme: "",
      pib: ""
    }as Banka);
}

  

  ngOnInit(): void {
    this.dobaviSve();
  }

  dobaviSve(){
    this.servis.dobaviBanku().subscribe(data=>this.items=data);
  }
  obrisi(id){
    this.servis.obrisiBanku(id).subscribe(data=>this.dobaviSve());
  }
  dodaj(item){
    this.servis.dodajBanku(item).subscribe(data=>this.dobaviSve());
  }
  detalj(id){
    this.router.navigate(["banka/bankaDetalj",{id:id}]);
  }

}
